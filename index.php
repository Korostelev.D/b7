<?php
header('Content-Type: text/html; charset=UTF-8');
$user = 'u23971';
$password = '3457564';
$db = new PDO('mysql:host=localhost;dbname=u23971', $user, $password, array(PDO::ATTR_PERSISTENT => true));
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $messages = array();
    print '<a href="admin.php">Войти как администратор</a>';
    if (!empty($_COOKIE['save']) ) {
        setcookie('save', '', 100000);
        setcookie('login', '', 100000);
        setcookie('pass', '', 100000);

        $messages[] = 'Спасибо, результаты сохранены.';

        if (!empty($_COOKIE['pass'])) {
            $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
                strip_tags($_COOKIE['login']),
                strip_tags($_COOKIE['pass']));
        }
        else
        if(empty($_COOKIE['update'])=='1')
        {
            header('Location:login.php');
        }
    }

    $errors = array();
    $errors_flag = FALSE;
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['birthyear'] = !empty($_COOKIE['birthyear_error']);
    $errors['radio1'] = !empty($_COOKIE['radio1_error']);
    $errors['radio2'] = !empty($_COOKIE['radio2_error']);
    $errors['bio'] = !empty($_COOKIE['bio_error']);
    $errors['checkbox'] = !empty($_COOKIE['checkbox_error']);
    $errors['super'] = !empty($_COOKIE['super_error']);

    if ($errors['fio']) {
        setcookie('fio_error', '', 100000);
        $messages[] = '<div class="error">Пусто или вы ввели недопустимые символы в поле имя!</div>';
        $errors_flag = TRUE;
    }

    if ($errors['email']) {
        setcookie('email_error', '', 100000);
        $messages[] = '<div class="error">Пусто или вы ввели недопустимые символы в поле email!</div>';
        $errors_flag = TRUE;
    }
    if ($errors['birthyear']) {
        setcookie('birthyear_error', '', 100000);
        $messages[] = '<div class="error">Пусто или вы еще не родились!</div>';
        $errors_flag = TRUE;
    }

    if ($errors['radio1']) {
        setcookie('radio1_error', '', 100000);
        $messages[] = '<div class="error">Вы не выбрали пол!</div>';
        $errors_flag = TRUE;
    }

    if ($errors['radio2']) {
        setcookie('radio2_error', '', 100000);
        $messages[] = '<div class="error">Вы не выбрали кол-во конечностей!</div>';
        $errors_flag = TRUE;
    }

    if ($errors['bio']) {
        setcookie('bio_error', '', 100000);
        $messages[] = '<div class="error">Вы не заполнили биографию!</div>';
        $errors_flag = TRUE;
    }

    if ($errors['checkbox']) {
        setcookie('checkbox_error', '', 100000);
        $messages[] = '<div class="error">Вы не отметили согласие на обработку!</div>';
        $errors_flag = TRUE;
    }
    if ($errors['super']) {
        setcookie('super_error', '', 100000);
        $messages[] = '<div class="error">Вы не выбрали способности!</div>';
        $errors_flag = TRUE;
    }


    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
    $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
    $values['birthyear'] = empty($_COOKIE['birthyear_value']) ? '' : strip_tags($_COOKIE['birthyear_value']);
    $values['radio1'] = empty($_COOKIE['radio1_value']) ? '' : strip_tags($_COOKIE['radio1_value']);
    $values['radio2'] = empty($_COOKIE['radio2_value']) ? '' : strip_tags($_COOKIE['radio2_value']);
    $values['bio'] = empty($_COOKIE['bio_value']) ? '' : strip_tags($_COOKIE['bio_value']);
    $values['checkbox'] = empty($_COOKIE['checkbox_value']) ? '' : strip_tags($_COOKIE['checkbox_value']);
    $values['super'] = empty($_COOKIE['super_value']) ? '' : strip_tags($_COOKIE['super_value']);


    if (!$errors_flag && !empty($_COOKIE[session_name()]) &&
        session_start() && !empty($_SESSION['login'])) {
        $user = 'u23971';
        $pass = '3457564';
        $db = new PDO('mysql:host=localhost;dbname=u23971', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

        $login_current=$_SESSION['login'];
        $result=$db->prepare("SELECT * from form  where login='$login_current'");
        $result->execute();
        $id=intval($_SESSION['uid']);
        $stat=$db->prepare("SELECT * from ability WHERE form_id='$id'");
        $stat->execute();
        $row=$stat->fetch(MYSQLI_ASSOC);
        $rows = $result->fetch(MYSQLI_ASSOC);

        $values['fio']=strip_tags($rows['name']);
        $values['email']=strip_tags($rows['email']);
        $values['birthyear']=strip_tags($rows['year']);
        $values['radio1']=strip_tags($rows['sex']);
        $values['radio2']=strip_tags($rows['limb']);
        $values['bio']=strip_tags($rows['bio']);
        $values['checkbox']=strip_tags($rows['checkbox']);
        $values['super']=strip_tags($row['ability_id']);
        print(" "). "<br>\r\n";

        printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);

    }

    include('form.php');
}
else {
    $errors = FALSE;
    if (preg_match("/[^(\w)|(\x7F-\xFF)|(\s)]/",$_POST['fio']) || empty($_POST['fio'])) {
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }

    else {
        setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
    }
    if (preg_match("/[^(\w)|(\@)|(\.)|(\-)]/",$_POST['email'])  || empty($_POST['email'])) {
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }

    else {
        setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
    }

    if (($_POST['birthyear']>2021) || empty($_POST['birthyear'])) {
        setcookie('birthyear_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }

    else {
        setcookie('birthyear_value', $_POST['birthyear'], time() + 30 * 24 * 60 * 60);
    }

    if ( empty($_POST['radio1'])) {
        setcookie('radio1_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }

    else {
        setcookie('radio1_value', $_POST['radio1'], time() + 30 * 24 * 60 * 60);
    }

    if ( empty($_POST['radio2'])) {
        setcookie('radio2_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }

    else {
        setcookie('radio2_value', $_POST['radio2'], time() + 30 * 24 * 60 * 60);
    }

    if ( empty($_POST['bio'])) {
        setcookie('bio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }

    else {
        setcookie('bio_value', $_POST['bio'], time() + 30 * 24 * 60 * 60);
    }

    if ( empty($_POST['checkbox'])) {
        setcookie('checkbox_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }

    else {
        setcookie('checkbox_value', $_POST['checkbox'], time() + 30 * 24 * 60 * 60);
    }

    $myselect = $_POST['super'];
    $flag=0;
    if(empty($myselect))
    {
        setcookie('super_error','1',time() + 24*60*60);
        $errors=TRUE;
        $flag=1;
    }

    if (!empty($myselect)) {
        foreach ($myselect as $ability) {
            if (!is_numeric($ability)) {
                setcookie('super_error', '2', time() + 24 * 60 * 60);
                $errors = TRUE;
                $flag=1;
                break;
            }

        }
    }
    if($flag==0) {
        setcookie('super_value', serialize($_POST['super']), time() + 30 * 24 * 60 * 60);
    }


    if ($errors) {
        header('Location: index.php');
        exit();
    } else {
   
        setcookie('fio_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('birthyear_error', '', 100000);
        setcookie('radio1_error', '', 100000);
        setcookie('radio2_error', '', 100000);
        setcookie('bio_error', '', 100000);
        setcookie('checkbox_error', '', 100000);
        setcookie('super_error', '', 100000);
    }

    if (!empty($_COOKIE[session_name()]) &&
        session_start() && !empty($_SESSION['login'])) {
        $user = 'u23971';
        $pass = '3457564';
        $db = new PDO('mysql:host=localhost;dbname=u23971', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
        $conn = new mysqli('localhost', $user, $pass);
        $name = mysqli_real_escape_string($conn,$_POST['fio']);
        $email = mysqli_real_escape_string($conn,$_POST['email']);
        $birthyear = mysqli_real_escape_string($conn,$_POST['birthyear']);
        $sex = mysqli_real_escape_string($conn,$_POST['radio1']);
        $limb = mysqli_real_escape_string($conn,$_POST['radio2']);
        $bio = mysqli_real_escape_string($conn,$_POST['bio']);
        $checkbox = mysqli_real_escape_string($conn,$_POST['checkbox']);
        $login = $_SESSION['login'];
        $id=intval($_SESSION['uid']);

        $user = 'u23971';
        $pass = '3457564';
        $db = new PDO('mysql:host=localhost;dbname=u23971', $user, $pass, array(PDO::ATTR_PERSISTENT => true));


        $stmt = $db->prepare("UPDATE form SET 
                name='$name',
                email='$email',
                year='$birthyear', 
                sex='$sex',
                limb='$limb', 
                bio='$bio',
                checkbox='$checkbox'
                WHERE login='$login'");
        $stmt->execute();

        $user = 'u23971';
        $pass = '3457564';
        $db = new PDO('mysql:host=localhost;dbname=u23971', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

        $myselect = $_POST['super'];
       
        $result1=$db->prepare("SELECT ability_id from ability  where form_id='$id'");
        $result1->execute();
        $flag=0;
        $rows = $result1->fetch();
        foreach ($rows as $row) {
            foreach ($myselect as $row1)
            {
                if($row!=$row1) $flag=1;
            }
        }


    } else {

        $login =substr(md5(uniqid(rand(),true)), 1, 5);
        $password = substr((uniqid(rand(),true)),2,5);

        setcookie('login', $login);
        setcookie('pass', $password);

        $user = 'u23971';
        $pass = '3457564';
        $db = new PDO('mysql:host=localhost;dbname=u23971', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

        $passwordbd=password_hash($password,PASSWORD_DEFAULT);
        $stmt = $db->prepare("INSERT INTO form (name,year,email,sex,limb,bio,checkbox,login,pass)
                                 VALUES (:fio,:birthyear,:email, :sex,:limb,:bio,:checkbox,:login,:pass)");
        $stmt -> execute(array(
            'fio'=>$_POST['fio'],
            'birthyear'=>$_POST['birthyear'],
            'email'=>$_POST['email'],
            'sex'=>$_POST['radio1'],
            'limb'=>$_POST['radio2'],
            'bio'=>$_POST['bio'],
            'checkbox'=>$_POST['checkbox'],
            'login'=>$login,
            'pass'=>$passwordbd));


        $form_id =  $db->lastInsertId();
        $myselect = $_POST['super'];
        if (!empty($myselect)) {
            foreach ($myselect as $ability) {
                if (!is_numeric($ability)) {
                    continue;
                }
                $stmt = $db->prepare("INSERT INTO ability (form_id, ability_id) VALUES (:form_id, :ability_id)");
                $stmt -> execute(array(
                    'form_id' => $form_id,
                    'ability_id' => $ability,
                ));
            }
        }
    }
    setcookie('save', '1');
    header('Location: ./');
}

